//============================================================================
// Name        : SimPro.cpp
// Author      : 0x0is1
// Version     : 0.1
// Copyright   : copyright by TAPs Inc.
// Description : client side bank project.
//============================================================================

#include <iostream>
#include <fstream>
#include <string>
#include <ctime>

using namespace std;

string fname;
string pass;

void banner(){
	system("clear");
	cout << "" <<endl;
	cout << "" <<endl;
	cout << "                               C-BANK" <<endl;
	cout << "                            [version 0.1]" <<endl;
	cout << "                           [TAPs Presents]   " <<endl;
	cout << "" <<endl;
	cout << "" <<endl;
}


void menu(){
	banner();
	cout << "    1)Open Account" << endl;
	cout << "    2)Withdraw Amount(Access token)" << endl;
	cout << "    3)Display All Account" << endl;
	cout << "    4)Delete Account" << endl;
	cout << "    5)Loan" << endl;
	cout << "    6)Transfer Amount" << endl;
	cout << "    7)check loan status" << endl;
	cout << "    8)Exit" << endl;
}
void defaults() {
	cout << "    1)menu" << endl;
			cout << "    2)Exit" << endl;
			int option;
			cin >> option;
			switch (option){
			case 1:
				system("./SimPro1");
				break;
			case 2:
				exit(0);
				break;
			default:
				cout << "    invalid option!" << endl;
				defaults();
			}
}
void registerme(){
	banner();
	string acno;
		cout << "    Enter Account No.:";
		cin >> acno;



	    cout << "    Full Name:";
		cin >> fname;

	    ofstream name;
		name.open(acno + "name.txt", ios::app);
	    name << fname << "";
	    name.close();
		cout << "" << endl;


		cout << "    password:";
		cin >> pass;

		ofstream passw;
		passw.open (acno + "pass.txt", ios::app);
		passw << pass << "";
		cout << "" << endl;
		passw.close();

		cout << "    confirm password:";
		string cpass;
		cin >> cpass;
		cout << "" << endl;
		if (pass == cpass){
			cout << "    Thanks for registering!" << endl;
			ofstream init_bal;
			int initb = 0;
			init_bal.open(acno + "bal.txt", ios::app);
			init_bal << initb;
			init_bal.close();
		}
		else if (pass == ""){
			remove((acno + "pass.txt").c_str());
		}
		else {
			cout << "    confirm password not matched!" << endl;
		}


		defaults();
}
void login(){
	banner();
	string acnor;
	cout << "    Enter you account number:";
	cin >> acnor;
	cout << "" << endl;
	ifstream passw;
	passw.open(acnor + "pass.txt", ios::app);
	string x;
	passw >> x;
	passw.close();

	cout << "    Enter you password:";
		cin >> pass;
		cout << "" << endl;
	if (x == pass){
		ifstream name;
		name.open(acnor + "name.txt", ios::app);
		string y;
		name >> y;
		cout << "    welcome " + y << endl;
		name.close();
		ifstream bal;
		bal.open(acnor + "bal.txt", ios::app);
		string z;
		bal >> z;
		cout << "    your current balance is:" + z << endl;
		bal.close();
	}
	else if (x == ""){
						cout << "    account does not exist!" << endl;
						remove((acnor + "pass.txt").c_str());
					}
					else {

						cout << "    incorrect password!" << endl;
					}

	defaults();
}

void first(){
	banner();
	cout << "    1)open a new account" << endl;
		cout << "    2)Log in registered account" << endl;
		cout << "    3)Go back" << endl;
		cout << "    4)Exit" << endl;
		int option;
		cin >> option;
		switch (option){
		case 1:
			registerme();
			break;
		case 2:
			login();
			break;
		case 3:
			menu();
			break;
		case 4:
			exit(1);
		default:
			cout << "    invalid option!" << endl;
		}
		defaults();
	}
void second(){
	banner();
	        string acnow;
			cout << "    Enter your account no here:";
			cin >> acnow;
			cout << "" << endl;
			ifstream passw;
			passw.open(acnow + "pass.txt", ios::app);
			string x;
			passw >> x;
			passw.close();
			cout << "    Enter you password:";
				cin >> pass;
				cout << "" << endl;
			if (x == pass){
				passw.close();
				cout << "    Enter amount you want to withdraw:";
				int amount;
				cin >> amount;

            ifstream withd;
            withd.open(acnow + "bal.txt");
            int m;
            withd >> m;
            withd.close();

            ofstream deduc;
            deduc.open(acnow + "bal.txt");
            deduc << m - amount;
            deduc.close();

            ofstream dwrite;
            dwrite.open(acnow + "wtoken.txt");
           	dwrite << rand();
            dwrite.close();

            ifstream dread;
            dread.open(acnow + "wtoken.txt");
            string k;
            dread >> k;
            dread.close();

            cout << "    show this token to bank to get money:" << k << endl;

			cout << "    0)show current balance" << endl;
			cout << "    1)go to main menu" << endl;
			cout << "    2)exit" << endl;

			string choice;
			cin >> choice;

			if(choice == "0"){
				ifstream readbl;
				 readbl.open(acnow + "bal.txt");
				 string z;
				 readbl >> z;
				 readbl.close();
                 cout << "    your current balance is: " + z << endl;
			}
			else if(choice == "1"){
				menu();
			}
			else if (choice == "2"){
				exit(1);
			}
			else {
				cout << "    invalid choice" << endl;

			}
		}
			else if (x == ""){
								cout << "    account does not exist!" << endl;
								remove((acnow + "pass.txt").c_str());
							}
							else {

								cout << "    incorrect password! " << endl;
							}
			defaults();
}
void third(){
	banner();
	system("zsh -c  'print -rl -- *name.txt(:r)'");
	defaults();
}
void fourth(){
	string acnod;
			cout << "    Enter account number to delete:";
			cin >> acnod;
			cout << "" << endl;
			ifstream passw;
			passw.open(acnod + "pass.txt", ios::app);
			string x;
			passw >> x;
			cout << "    Enter account's password:";
				cin >> pass;
				cout << "" << endl;
			if (x == pass){
				remove((acnod + "pass.txt").c_str());
				remove((acnod + "bal.txt").c_str());
				remove((acnod + "name.txt").c_str());
			}
			else {
				cout << "    incorrect password!" << endl;
			}
			defaults();
}
void fifth(){
	cout << "    Enter your account no here:";
	string acl;
					cin >> acl;
					cout << "" << endl;
					ifstream passw;
					passw.open(acl + "pass.txt", ios::app);
					string x;
					passw >> x;
					cout << "    Enter you password:";
						cin >> pass;
						cout << "" << endl;
						passw.close();

						string null;
						ifstream f;
						f.open(acl + "loan.txt");
						f >> null;
						f.close();

						if (null == ""){


					if (x == pass){
						time_t then = time(0);
							tm *ltm = localtime(&then);
							int year = 1900 + ltm ->tm_year;

							int inter = 5;
							cout << "    interest per year is now:" << inter << "%" << endl;

							cout << "    Enter amount here:" << endl;
							int amount;
							cin >> amount;

							ofstream bald;
							bald.open(acl + "loan.txt", ios::app);
							bald << year << endl;
							bald << amount << endl;
							bald.close();

							int z;
							ifstream gald;
							gald.open(acl + "bal.txt");
							gald >> z;
							gald.close();


							ofstream acnl;
							acnl.open(acl + "bal.txt");
							acnl << z + amount;
							acnl.close();
							cout << "    loan is approved successfully! check acc." << endl;

					}
					else if(x == ""){
						cout << "    account doesn't exist" << endl;
						remove((acl + "pass.txt").c_str());

											}
					else {
						cout << "    incorrect password!" << endl;
					}


}

						else{
							cout << "    you have already applied for the loan!" << endl;
						}
						defaults();
}
void sixth(){
	 string acnow;
				cout << "    Enter your account no here:";
				cin >> acnow;
				cout << "" << endl;
				ifstream passw;
				passw.open(acnow + "pass.txt", ios::app);
				string x;
				passw >> x;
				cout << "    Enter you password:";
					cin >> pass;
					cout << "" << endl;
					passw.close();
				if (x == pass){
					cout << "    Enter amount you want to transfer:";

					int amount;
					cin >> amount;

	            ifstream trans;
	            trans.open(acnow + "bal.txt");
	            int m;
	            trans >> m;
	            trans.close();

	            ofstream deduc;
	            deduc.open(acnow + "bal.txt");
	            deduc << m - amount;
	            deduc.close();

                cout << "    Enter reciever's account no:";
                string recan;
                	 cin >> recan;

                	 trans.open(recan + "bal.txt");
                	 int z;
                	 trans >> z;
                	 trans.close();

                     int y = amount + z;
                     cout << y << endl;

                     deduc.open(recan + "bal.txt");

                    	 deduc << y;
                    	 deduc.close();

				cout << "    0)show current balance" << endl;
				cout << "    1)go to main menu" << endl;
				cout << "    2)exit" << endl;

				string choice;
				cin >> choice;

				if(choice == "0"){
					ifstream readbl;
					 readbl.open(acnow + "bal.txt");
					 string z;
					 readbl >> z;
					 readbl.close();
	                 cout << "    your current balance is: " + z << endl;
				}
				else if(choice == "1"){
					menu();
				}
				else if (choice == "2"){
					exit(1);
				}
				else {
					cout << "    invalid choice" << endl;

				}
			}
				else if (x == ""){
					cout << "    account does not exist!" << endl;
					remove((acnow + "pass.txt").c_str());
				}
				else {

					cout << "    incorrect password" << endl;
				}

				defaults();
}
void seventh(){
	exit(1);
}
void eighth() {

	cout << "    Enter your account no here:";
		string aci;
						cin >> aci;
						cout << "" << endl;
						ifstream passw;
						passw.open(aci + "pass.txt", ios::app);
						string x;
						passw >> x;
						cout << "    Enter you password:";
							cin >> pass;
							cout << "" << endl;
							passw.close();
						if (x == pass){
							time_t now = time(0);
								tm *ntm = localtime(&now);
								int yearn = 1900 + ntm ->tm_year;

								int p,q;
								ifstream r;
								r.open(aci + "loan.txt");
								r >> p;
								r >> q;
								r.close();

								int t = yearn -p;

								int calc = (q*t)/20;

								int am = q + calc;

								cout << "    your current total amount with interest " << calc <<" is: " << am << endl;
						}
						else if (x == ""){
							cout << "    account does not exist!" << endl;
							remove((aci + "pass.txt").c_str());
						}
						else {
							cout << "    wrong password or not registered" << endl;
						}
						defaults();
}

int main() {

    menu();
    cout << "    select a option from following:";
    	int option;
    	cin >> option;
    	switch (option) {
    	case 1:
    		first();
    		break;
    	case 2:
    		second();
    		break;
    	case 3:
    		third();
    		break;
    	case 4:
    		fourth();
    		break;
    	case 5:
    		fifth();
    		break;
    	case 6:
    		sixth();
    		break;
    	case 7:
    		eighth();
    		break;

    	case 8:
    		seventh();
    		break;

    	default:
    		cout << "    invalid entry" << endl;
    		defaults();
    	}

    	return 0;
}
